#pragma once
#include "Primitive.h"
class Point
{
public:
	Point(long _x=0, long _y=0) : x((MemberType)_x), y((MemberType)_y) {}
	MemberType x, y;

	long getX() {
		return (long)x;
	}

	long getY() {
		return (long)y;
	}

	std::string toString() {
		std::string s;
		s = std::to_string((long)x) + ", " + std::to_string((long)y) + std::string("\n");
		return s;
	}
};

