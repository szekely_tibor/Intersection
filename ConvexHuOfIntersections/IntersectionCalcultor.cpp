#include "IntersectionCalcultor.h"
#include <limits>
using namespace std;
MemberType IntersectionCalcultor::epsilon = 1;
// LINE-LINE INTERSECTION ------------------------------------------------------------------
// Source: https://www.geeksforgeeks.org/program-for-point-of-intersection-of-two-lines/
bool IntersectionCalcultor::intersect(const Line* l1, const Line* l2, vector<Point>& result) {
	MemberType determinant = l1->a * l2->b - l2->a * l1->b;
	if (determinant == 0) {// the lines are paralell
		Point p(LONG_MAX, LONG_MAX);
		result.push_back(p);
		return true;
	}
	else {
		MemberType x = (l2->b * l1->c - l1->b * l2->c) / determinant;
		MemberType y = (l1->a * l2->c - l2->a * l1->c) / determinant;
		Point p((long)x, (long)y);
		result.push_back(p);
		return false;
	}
}

// CIRCLE-CIRCLE INTERSECTION ----------------------------------------------------------------
// Source: https://www.xarg.org/2016/07/calculate-the-intersection-points-of-two-circles/
bool IntersectionCalcultor::intersect(const Circle *c1, const Circle *c2, vector<Point>& result) {
	MemberType distance = sqrt((c2->x - c1->x) * (c2->x - c1->x) + (c2->y - c1->y) * (c2->y - c1->y));
	if (distance < (c1->r + c2->r) && distance >= abs(c2->r - c1->r)) {
		MemberType ex = (c2->x - c1->x) / distance;
		MemberType ey = (c2->y - c1->y) / distance;

		MemberType x = (c1->r * c1->r - c2->r * c2->r + distance * distance) / (2 * distance);
		MemberType y = sqrt(c1->r * c1->r - x * x);
		Point p1((long)(c1->x+x*ex-y*ey), (long)(c1->y+x*ey+y*ex));
		Point p2((long)(c1->x+x*ex+y*ey), (long)(c1->y+x*ey-y*ex));
		result.push_back(p1);
		result.push_back(p2);		
		return true;
	}
	else {
		result.push_back(Point(LONG_MAX, LONG_MAX));
		result.push_back(Point(LONG_MAX, LONG_MAX));
		return false;
	}

}
// LINE-CIRCLE INTERSECTION ----------------------------------------------------------------
// Source: https://cp-algorithms.com/geometry/circle-line-intersection.html
bool IntersectionCalcultor::intersect(const Line *l, const Circle *c, vector<Point>& result) {
	MemberType x0 = -l->a * l->c / (l->a * l->a + l->b * l->b);
	MemberType y0 = -l->b * l->c / (l->a * l->a + l->b * l->b);
	if (l->c * l->c > c->r * c->r * (l->a * l->a + l->b * l->b) + EPS){
		// No intersection point
		return false;
	}
	else if (abs(l->c * l->c - c->r * c->r * (l->a * l->a + l->b * l->b)) < EPS) { 
		// One intersection point
		result.push_back(Point((long)x0, (long)y0));
		return true;
	}
	else {
		// Two intersection point
		MemberType d = c->r * c->r - l->c * l->c / (l->a * l->a + l->b * l->b);
		MemberType mult = sqrt(d / (l->a * l->a + l->b * l->b));
		MemberType ax, ay, bx, by;
		ax = x0 + l->b * mult;
		bx = x0 - l->b * mult;
		ay = y0 - l->a * mult;
		by = y0 + l->a * mult;
		result.push_back(Point((long)ax, (long)ay));
		result.push_back(Point((long)bx, (long)by));
		return true;
	}
}
