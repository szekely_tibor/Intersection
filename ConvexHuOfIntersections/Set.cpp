#include "Set.h"
#include "Line.h"
#include "Circle.h"
#include "Point.h"
#include "IntersectionCalcultor.h"
#include <algorithm>


Set::Set(){

}

void Set::getPrimitives(bool fromFile) {
	ifstream infile;
	int numPrimitives;
	infile.open("test.dat");
	if (fromFile) {
		infile >> numPrimitives;
		for (int i = 0; i < numPrimitives; i++) {
			char pType;
			infile >> pType;
			if (pType == 'L') {
				long x0, y0, x1, y1;
				infile >> x0;
				infile >> y0;
				infile >> x1;
				infile >> y1;
				Line* l = new Line(x0, y0, x1, y1);
				primitives.push_back(l);
			}
			else if (pType == 'C') {
				long x, y, r;
				infile >> x;
				infile >> y;
				infile >> r;
				Circle* c = new Circle(x, y, r);
				primitives.push_back(c);
			}
		}
		infile.close();
	}
	else {
		cin >> numPrimitives;
		for (int i = 0; i < numPrimitives; i++) {
			char pType;
			cin >> pType;
			if (pType == 'L') {
				long x0, y0, x1, y1;
				infile >> x0;
				infile >> y0;
				infile >> x1;
				infile >> y1;
				Line *l = new Line(x0, y0, x1, y1);
				primitives.push_back(l);
			}
			else if (pType == 'C') {
				long x, y, r;
				infile >> x;
				infile >> y;
				infile >> r;
				Circle *c = new Circle(x, y, r);
				primitives.push_back(c);
			}
		}
	}
	points.reserve(numPrimitives * 2); // my guess
}

void Set::writeOutPrimitives() {
	cout << "-----------------------------\n";
	cout << "        PRIMITIVES\n";
	cout << "-----------------------------\n";
	cout << primitives.size() << endl;
	for (int i = 0; i < primitives.size(); i++) {
		cout << primitives[i]->toString();
	}
}

void Set::writeOutPoints() {
	ofstream ofile;
	ofile.open("allpoints.dat");
	ofile << points.size();
	cout << "-----------------------------\n";
	cout << "   POINTS OF INTERSECTIONS\n";
	cout << "-----------------------------\n";

	cout << points.size() << endl;
	for (int i = 0; i < points.size(); i++) {
		ofile << points[i].toString();
		cout << points[i].toString();
	}
}

void Set::writeOutHullPoints() {
	ofstream ofile;
	ofile.open("hullpoints.dat");
	ofile << hullPoints.size();
	cout << "-----------------------------\n";
	cout << "   POINTS OF CONVEX HULL\n";
	cout << "-----------------------------\n";
	cout << hullPoints.size() << endl;
	for (int i = 0; i < hullPoints.size(); i++) {
		ofile << hullPoints[i].toString();
		cout << hullPoints[i].toString();
	}
}

void Set::calcIntersections() {
	points.clear();
	for (int i = 0; i < primitives.size(); i++) {
		for (int j = i; j < primitives.size(); j++) {
			if (i != j) {
				Primitive *p1 = primitives[i];
				Primitive *p2 = primitives[j];
				if (p1 != p2) {
					vector<Point> is = intersect(p1, p2);
					points.insert(points.end(), is.begin(), is.end());
				}
			}
		}
	}
}

vector<Point> Set::intersect(const Primitive *p1, const Primitive *p2) {
	vector<Point> pv;
	short p = perm(p1->t, p2->t);
	
	switch (p) {
		case perm('L', 'L'):
			// Line - Line intersection
			IntersectionCalcultor::intersect((Line*)p1, (Line*)p2, pv);
			break;
		case perm('L', 'C'):
			// Line - Circle intersection
			IntersectionCalcultor::intersect((Line*)p1, (Circle*)p2, pv);
			break;
		case perm('C', 'L'):
			// Line - Circle intersection
			IntersectionCalcultor::intersect((Line*)p2, (Circle*)p1, pv);
			break;
		case perm('C', 'C'):
			// Circle - Circle intersection
			IntersectionCalcultor::intersect((Circle*)p1, (Circle*)p2, pv);
			break;
	}
	return pv;
}

Point Set::secondTop(stack<Point>& stk) {
	Point tempPoint = stk.top();
	stk.pop();
	Point res = stk.top();    //get the second top element
	stk.push(tempPoint);      //push previous top again
	return res;
}

int direction(Point a, Point b, Point c) {
	long val = (b.getY() - a.getY()) * (c.getX() - b.getX()) - (b.getX() - a.getX()) * (c.getY() - b.getY());
	if (val == 0)
		return 0;    //colinear
	else if (val < 0)
		return 2;    //anti-clockwise direction
	return 1;    //clockwise direction
}

int Set::squaredDist(Point p1, Point p2) {
	return ((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

Point Set::p0;

int Set::comparePoints(const void* a, const void* b) {
	Point* p1 = (Point*)a;
	Point* p2 = (Point*)b;
	int dir = direction(Set::p0, *p1, *p2);
	if (dir == 0)
		return (squaredDist(p0, *p2) >= squaredDist(p0, *p1)) ? -1 : 1;
	return (dir == 2) ? -1 : 1;
}
// Graham Scan Algorithm to Find the Convex Hull
// source: https://www.tutorialspoint.com/cplusplus-program-to-implement-graham-scan-algorithm-to-find-the-convex-hull
void Set::findHullPoints() {
	hullPoints.clear();
	// TODO: implement the algorithm
#if 0
	for (int i = 0; i < points.size(); i++) {
		Point &p = points[i];
		hullPoints.push_back(p);
	}
#else
	long minY = points[0].getY();
	long min = 0;
	for (int i = 1; i < points.size(); i++) {
		long y = points[i].getY();
		if (y < minY || minY == y && points[i].getX() < points[min].getX()) {
			minY = points[i].getY();
			min = i;
		}
	}
	Point temp = points[0];          // SWAP
	points[0] = points[min];
	points[min] = temp;
	p0 = points[0];
	std::qsort(&points[1], points.size() - 1, sizeof(Point), Set::comparePoints);
	int arrSize = 1;
	for (int i = 1; i < points.size(); i++) {
		while (i < points.size()-1 && direction(p0, points[i], points[i + 1]) == 0)
			i++;
		points[arrSize] = points[i];
		arrSize++;
	}
	if (arrSize < 3)
		return;
	stack<Point> stk;
	stk.push(points[0]);
	stk.push(points[1]);
	stk.push(points[2]);
	for (int i = 3; i < arrSize; i++) {
		while (direction(secondTop(stk), stk.top(), points[i]) != 2)
			stk.pop();
		stk.push(points[i]);
	}
	while (!stk.empty()) {
		hullPoints.push_back(stk.top());    //add points from stack
		stk.pop();
	}
#endif
}
